<?php

/* CLIENT CONFIGURATION */

/* for logging info */
define('DEF_CLIENT_LABEL', 'localhost');

/* the local url containing the network-test directory */
define('DEF_CLIENT_URL', '127.0.0.1');

/* for logging info */
define('DEF_REMOTE_LABEL', 'digices');

/* the server url containing the network-test directory */
define('DEF_REMOTE_URL', 'www.digices.com');

/* size of file in MB (10, 20, or 40) */
define('TEST_FILE_SIZE', '10');

/* 1 MB = 8,000 bits -> 10 MB = 80 Mb */

/* size of file in MB (80, 160, or 320) depending on file size */
define('MBPS_DIVISOR', '80');

/* time to wait between latency checks */
define('WAIT_MS', '200');
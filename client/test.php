<?php

require_once('config.php');

// calculate the download latency by downloading a 1-byte file
function download_latency()
{
    $start = microtime(true);
    
    $latency_test = file_get_contents('http://'.$_GET['server_ip'].'/network-test/server/downloads/latency.txt?x='.$randoms=rand(intval(100000000000), intval(9999999999999)));
    
    $end = microtime(true);
    
    if ($latency_test == '1') {

        return $end - $start;

    } else {
    
        return false;
    }
    
}

// calculate the upload latency by uploading a 1-byte file
function upload_latency()
{

    $file = __DIR__.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.'latency';

    $start = microtime(true);
    
    $command = 'curl -F "uploadedfile=@'.$file.'" http://'.$_GET['server_ip'].'/network-test/server/upload.php';

    if (substr(shell_exec($command), 0, 1) == 'Y') {

        $end = microtime(true);

        return floatval($end - $start);

    } else {
    
        return false;
    }
    
    
}

// take a mumber of readings to reduce margin of error
function download_latency_average()
{

    if ($latency1 = download_latency()) {
        usleep(WAIT_MS);
    } else {
        return false;
    }

    if ($latency2 = download_latency()) {
        usleep(WAIT_MS);
    } else {
        return false;
    }

    if ($latency3 = download_latency()) {
        usleep(WAIT_MS);
    } else {
        return false;
    }

    if ($latency4 = download_latency()) {
        usleep(WAIT_MS);
    } else {
        return false;
    }

    if ($latency5 = download_latency()) {
        usleep(WAIT_MS);
    } else {
        return false;
    }

    // don't count the first one
    return ($latency2 + $latency3 + $latency4 + $latency5) / 4;

}

// take a mumber of readings to reduce margin of error
function upload_latency_average()
{

    if ($latency1 = upload_latency()) {
        usleep(WAIT_MS);
    } else {
        return false;
    }

    if ($latency2 = upload_latency()) {
        usleep(WAIT_MS);
    } else {
        return false;
    }

    if ($latency3 = upload_latency()) {
        usleep(WAIT_MS);
    } else {
        return false;
    }

    if ($latency4 = upload_latency()) {
        usleep(WAIT_MS);
    } else {
        return false;
    }

    if ($latency5 = upload_latency()) {
        usleep(WAIT_MS);
    } else {
        return false;
    }

    // don't count the first one
    return ($latency2 + $latency3 + $latency4 + $latency5) / 4;

}

// download a file from the remote server
function download()
{

    if ($latency = download_latency_average()) {

        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'download_latency.txt', $latency);

    } else {
    
        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'download_latency.txt', 'UNDEFINED');

    }

    $start = microtime(true);
    
    $file = file_get_contents('http://'.$_GET['server_ip'].'/network-test/server/downloads/'.TEST_FILE_SIZE.'mb.txt?x='.$randoms=rand(intval(100000000000), intval(9999999999999)));
    file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'test_file_size.txt', TEST_FILE_SIZE);
    
    if (substr($file, 0, 5) == 'b6589') {
    
        $end = microtime(true);

		$transfer_time = $end - $start - $latency;

        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'download_time.txt', $transfer_time);

		$mbps = MBPS_DIVISOR / $transfer_time;
		
        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'download_speed.txt', round($mbps,2));

    } else {
    
        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'download_time.txt', 'UNDEFINED');

    }
    
}

// upload a local file to the remote server
function upload()
{

    $file = __DIR__.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.TEST_FILE_SIZE.'mb';

    if ($latency = upload_latency_average()) {

        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'upload_latency.txt', $latency);

    } else {
    
        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'upload_latency.txt', 'UNDEFINED');

    }

    $start = microtime(true);

    $command = 'curl -F "uploadedfile=@'.$file.'" http://'.$_GET['server_ip'].'/network-test/server/upload.php';

    if (substr(shell_exec($command), 0, 1) == 'Y') {
    
        $end = microtime(true);

		$transfer_time = $end - $start - $latency;

        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'upload_time.txt', $transfer_time);

		$mbps = MBPS_DIVISOR / $transfer_time;
		
        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'upload_speed.txt', round($mbps,2));

    } else {
    
        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'upload_time.txt', 'UNDEFINED');
        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.'upload_speed.txt', '---');

    }

    return 'DONE!';
    
}

file_put_contents('tmp/download_latency.txt', '-');
file_put_contents('tmp/download_speed.txt', '-');
file_put_contents('tmp/download_time.txt', '-');
file_put_contents('tmp/test_file_size.txt', '-');
file_put_contents('tmp/upload_latency.txt', '-');
file_put_contents('tmp/upload_speed.txt', '-');
file_put_contents('tmp/upload_time.txt', '-');

echo download();
echo upload();

file_get_contents('http://'.$_GET['server_ip'].'/network-test/server/allclear.php?all_clear=1');
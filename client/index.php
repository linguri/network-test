<?php

require_once('config.php');

echo '<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <title>Network Speed Test Utility</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="row" style="text-align:center;background-color:#EEE;margin-bottom:12px;">
      <h1 style="color:#2F70B1;">Network Test Utility</h1>
    </div>
    <div class="container">
    <div class="row"><div class="col-md-4">&nbsp;</div><div class="col-md-4">
    <form>
	  <div class="form-group">
		<p class="help-block">Run this on your local server (http://localhost)</p>
	  </div>
	  <div class="form-group">
      <label for="client_name">Client Label</label>
      <input type="text" class="form-control" name="client_name" id="client_name" value="'.DEF_CLIENT_LABEL.'">
    </div>
	  <div class="form-group">
      <label for="client_ip">Client URL</label>
      <input type="text" class="form-control" name="client_url" id="client_url" value="'.DEF_CLIENT_URL.'">
    </div>
	  <div class="form-group">
      <label for="server_name">Server Label</label>
      <input type="text" class="form-control" name="server_name" id="server_name" value="'.DEF_REMOTE_LABEL.'">
    </div>
	  <div class="form-group">
      <label for="server_ip">Server URL</label>
      <input type="text" class="form-control" name="server_ip" id="server_url" value="'.DEF_REMOTE_URL.'">
    </div>
	  <div class="form-group">
		<p class="help-block">Test Connection Speed:</p>
	  </div>
	  <button type="button" class="btn btn-primary" onclick="loadDoc()">Begin</button>
	</form>
    </div><div class="col-md-4">&nbsp;</div></div>
    <div class="row"><hr></div>
    <div class="row"><div class="col-md-4">&nbsp;</div><div class="col-md-4">
    <table class="table table-striped">
      <thead>
        <tr><th>Parameter</th><th style="text-align:right;">Value</th></tr>
      </thead>
      <tbody>
        <tr><td>File Size (MB)</td><td id="test_file_size" class="val" style="text-align:right;">&nbsp;</td></tr>
        <tr><td>Latency</td><td id="download_latency" class="val" style="text-align:right;">&nbsp;</td></tr>
        <tr><td>Download Time</td><td id="download_time" class="val" style="text-align:right;">&nbsp;</td></tr>
        <tr><td>Download Mbps</td><td id="download_speed" class="val" style="font-weight:bold;text-align:right;">&nbsp;</td></tr>
        <tr><td>Latency</td><td id="upload_latency" class="val" style="text-align:right;">&nbsp;</td></tr>
        <tr><td>Upload Time</td><td id="upload_time" class="val" style="text-align:right;">&nbsp;</td></tr>
        <tr><td>Upload Mbps</td><td id="upload_speed" class="val" style="font-weight:bold;text-align:right;">&nbsp;</td></tr>
      </tbody>
    </table>
    <div id="message">&nbsp;</div>
    </div><div class="col-md-4">&nbsp;</div></div>
    <div>
	<script>
	
	function update(div) {
		var client = new XMLHttpRequest();
		client.open("GET", "tmp/" + div + ".txt");
		client.onreadystatechange = function() {
		  document.getElementById(div).innerHTML = client.responseText;
		}
		client.send();
	}
	
	function updateAll() {
	  update("test_file_size");
	  update("download_latency");
	  update("download_time");
	  update("download_speed");
	  update("upload_latency");
	  update("upload_time");
	  update("upload_speed");
	}
	
	function loadDoc() {
	  var messagediv = document.getElementById("message");
	  messagediv.innerHTML = "Please wait. Test will take at least '.(intval(floatval(WAIT_MS) / 100) + 1).' seconds";
	  var server_url = document.getElementById("server_url").value;
	  var client_url = document.getElementById("client_url").value;
	  var base_url = "http://" + client_url + "/network-test/client/";
	  var get_url = base_url + "test.php?server_ip=" + server_url + "&file_size='.TEST_FILE_SIZE.'";
	  var xhttp = new XMLHttpRequest();
	  xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
		   messagediv.innerHTML = xhttp.responseText;
		}
	  };
	  xhttp.open("GET", get_url, true);
	  xhttp.send();
      
	  setInterval(function(){
		updateAll() // this will run after every second
	  }, 100);

	}
	</script>
  </body>
</html>';

NETWORK TEST
(c) Copyright 2015 Digices, Inc

A simple network speed testing utility written in PHP

This test is designed to be run between two servers running Apache. If the 'network-test'
repository is cloned into the root directory of the server, the config files should not need to be edited.

You may need to edit post_max_size and upload_max_filesize in the php.ini file on the remote server.

The 'client' directory contains the application and data to be run from the source server.
You can edit the config.php file to set the defaults for each field in the web form.

The 'server' directory contains the application and data to be hosted on the target
server.

You can clone the entire repository onto both machines and run the tests in both directions.

----
APPLICATION LOGIC

The client first checks how long it takes to connect to the server and download/upload
a one-byte file four times in 1 second intervals, then takes the average of the last three
results and averages them to determine server latency.

The client then downloads a file (size determined by TEST_FILE_SIZE) from the server and times the process to determine download speed.

The client then uploads the same size file to the server and times the process to
determine upload speed.

The random numbers appended to the urls are to prevent caching from coloring the results.


<?php

// required by date();
date_default_timezone_set('America/Denver');

// generate a random number to append to the filename
$rand = rand(intval(10000), intval(99999));

// specify the relative path to the uploads directory
$uploads_dir = __DIR__.DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR;

// add the filename to the path
$target_path = $uploads_dir.basename( $_FILES['uploadedfile']['name']).'_'.$rand.'.txt';

// start the timer
$start_time = microtime(true);

// attempt file upload
if (move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path)) {

    $end_time = microtime(true);
    
    $transfer_time = $end_time - $start_time;

    echo 'Y';

} else {

    echo 'N';

}

?>

